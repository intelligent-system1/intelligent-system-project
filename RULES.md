# Logical Rules

- mammal has mammary glands 
- animal that has mammary glands produces milk
#
- primate has two hands
- primate has two legs
- primate eats meat
- primate eats vegetable
#
- animal that eats vegetable has molar teeth
#
- bird has wings
- bird lays egg
- bird has fur
- animal that has wings can fly
#
- animals have legs except snakes -----------  
- anything that has legs can move / crawl ---------
#
- Animal that has no backbone has no kidneys
#
- vertebrata has backbone
- animal that has backbone has kidneys
- animal that has backbone has endoskeleton
- animal that has kidney has flesh
#
- carnivora eats meat
- animal that eats meat has fangs
#
- Reptilia lays eggs
- Reptilia has scales
- Reptilia is cold-blooded
- Tetrapoda that is not serpentes has four limbs
#
- Arachnida has Appendage
- Arachnida has no backbone
- animal that has eight appendage has exoskeleton
- animal that has eight appendage eats insects
- animal that has eight appendage has mandible
#
- Insect has mandible
- Insect has exoskeleton
- Insect has no backbone
#
- Crustacea has exoskeleton
- Crustacea has a pair of antennae
#
- Decapoda have 5 pair of limbs
#
- Octopoda have 4 pair of limbs
#
- Cephalopod have arms/tentacles attached to it's head
- Cephalopod can squirt ink when threatened
#   
- Amphibia can live both on land and in water
- Amphibia go through metamorphosis stages
#
- Serpentes has no legs and it moves by using its belly
#
- cephalopod OR crustacea lives underwater