'''
Wikidata Animal Taxon and Information Extractor

created by Fransiscus Emmanuel Bunaren
'''

#Import Wikidata Library for Python
from wikidata.client import Client

def hasTaxonomyData(wiki_id):
    #Initializing Wikidata Client
    client = Client()

    #Load Object Entity
    entity = client.get(wiki_id, load=True)
    
    try:
        data = entity[client.get('P171')]
        return True
    except:
        return False


def animalData(wiki_id):
    #Initializing Wikidata Client
    client = Client()

    #Load Object Entity
    entity = client.get(wiki_id, load=True)

    returned_data = dict()
    returned_data['name'] = str(entity.label)
    returned_data['description'] = str(entity.description)
    try:
        returned_data['image'] = str(entity[client.get('P18')].image_url)
    except:
        returned_data['image'] = str("/static/undraw_unicorn_dp2f.svg")
    returned_data['taxon'] = []

    data = entity[client.get('P171')]
    while True:
        try:
            desc = str(data.description)
            returned_data['taxon'].append({'label' : desc.split()[0].ljust(15) , 'value' : str(data.label) , "description" : desc })
            data = data[client.get('P171')]
        except:
            break
    
    try:
        data = entity[client.get('P225')]
        returned_data['taxon_name'] = str(data)
    except:
        returned_data['taxon_name'] = "-"

    return returned_data
