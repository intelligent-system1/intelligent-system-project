from django.shortcuts import render
import taxonomy
import os
import json
import requests
from random import choice

# Create your views here.
def index(request):
    return render(request, 'homepage.html')

def search(request):
    # Load Animal Suggestion TXT File
    with open("./Animal-words.txt") as f:
        suggest = f.read()
    suggest = suggest.split("\n")
    suggest = choice(suggest)

    response = {'name' : '' , 'wiki_id' : '','suggest' : suggest}
    if request.method == "POST":
        query = request.POST['animal']
        #If user inputs keyword, then we will search it directly from Wikidata
        if query[0] != "Q" and not str.isdigit(query[1:]):
            found = False
            search_api = requests.get("https://www.wikidata.org/w/api.php?action=query&list=search&srsearch=" + request.POST['animal'] + "&utf8=&format=json")
            search = json.loads(search_api.text)['query']['search']
            for i in search:
                if taxonomy.hasTaxonomyData(i['title']):
                    query = i['title']
                    found = True
                    break
        
            if not found:
                # Search from a-z-animal
                az = requests.get("https://a-z-animals.com/animals/" + query).text
                
                if ("File Not Found" in az or "Scientific Name</a>" not in az):
                    return render(request, 'not-found.html')

                az = az.split("Scientific Name</a>")[1]
                az = az.split("<td>")[1]
                az = az.split("</td>")[0]
                az = az.split(",")[0]
                query = az.replace(" ","+")

                # Match it again with wikidata
                search_api = requests.get("https://www.wikidata.org/w/api.php?action=query&list=search&srsearch=" + query + "&utf8=&format=json")
                search = json.loads(search_api.text)['query']['search']
                for i in search:
                    if taxonomy.hasTaxonomyData(i['title']):
                        query = i['title']
                        print("Has Taxon :" ,taxonomy.hasTaxonomyData(i['title']))
                        found = True
                        break

        #Implementing Cache for better performance
        try:
            os.mkdir("./cache")
        except:
            pass
        cache_path = "./cache/" + query + ".json"
        if os.path.isfile(cache_path):
            with open(cache_path) as f:
                response = json.load(f)
        else:
            response = taxonomy.animalData(query)
            with open(cache_path,"w+") as f:
                f.write(json.dumps(response))
            
        response['wiki_id'] = query

        #Forward Chaining
        #by Fransiscus Emmanuel Bunaren

        #Populate Rules
        rules = dict()
        
        rules["taxon_cephalopod | taxon_bivalvia"] = ["lives underwater"]

        rules["taxon_carnivora"] = ["eat meat"]
        rules["eat meat"] = ["have fangs"]

        rules["taxon_mammal"] = ["have mammary glands"]
        rules["have mammary glands"] = ["breastfeed its children"]
        rules["breastfeed its child"] = ["produce milk"]

        rules["taxon_vertebrata"] = ["have backbone"]
        rules["have backbone"] = ["have kidneys","have endoskeleton"]
        rules["have kidneys"] = ["have meat"]

        rules["taxon_primates"] = ["have two hands", "have two legs", "eat meat", "eat plants"]
        rules["eat plants"] = ["have molar teeth"]

        rules["taxon_bird"] = ["have wings", "lay eggs", "have fur"]
        rules["have wings"] = ["can fly"]

        rules["taxon_reptilia"] = ["lay eggs", "have scales", "are cold-blooded"]

        rules["taxon_toxicofera"] = ["are venomous"]
        rules["are venomous"] = ["are dangerous"]
        rules["taxon_dinosaur"] = ["are dinosaurs"]
        rules["are dinosaurs"] = ["are extinct"]

        rules['taxon_arachnida'] = ["have eight appendages"]
        rules["have eight appendages"] = ['have exoskeleton','eat insects','have mandibles']

        rules['taxon_insect'] = ['have mandibles','have exoskeleton','have no backbone']
        rules['have no backbone'] = ['have no kidneys']

        rules['taxon_crustacea'] = ['have exoskeleton','have a pair of antennae']

        rules['taxon_decapoda'] = ['have 5 pair of limbs']

        rules['taxon_octopoda'] = ['have 4 pair of limbs']

        rules['taxon_cephalopod'] = ["have arms/tentacles attached to it's head", 'can squirt ink when threatened']

        rules['taxon_amphibia'] = ['can live both on land and in water','goes through metamorphosis stages']

        rules['taxon_serpentes'] = ['moves using its belly','has no legs']

        #Advanced Rules (using operators)
        rules["taxon_tetrapoda & !taxon_serpentes"] = ["have four limbs"]
        
        #Populate Facts
        facts = ["taxon_" + i['value'].lower() for i in response['taxon']]
        explored_facts = []
        
        new_facts = 1
        #Rules with operators
        #It can handle negation (!), AND (&) , OR (|)
        for tmp_i in rules:
            if (" | " in tmp_i) or (" & " in tmp_i) or (" ! " in tmp_i):
                if " & " in tmp_i:
                    valid = True
                    tmp = tmp_i.split(" & ")
                    for h in tmp:
                        if "!" == h[0] and h[1:] in facts:
                            valid = valid and False
                        elif "!" == h[0] and h[1:] in explored_facts:
                            valid = valid and False
                        elif h not in facts and "!" != h[0]:
                            valid = valid and False
                    if valid:
                        facts.append(rules[tmp_i])
                        new_facts += 1
                if " | " in tmp_i:
                    tmp = tmp_i.split(" | ")
                    for h in tmp:
                        if "!" == h[0] and h[1:] not in facts:
                            facts.append(rules[tmp_i])
                            new_facts += 1
                            continue
                        elif "!" == h[0] and h[1:] not in explored_facts:
                            facts.append(rules[tmp_i])
                            new_facts += 1
                            continue
                        elif h in facts:
                            facts.append(rules[tmp_i])
                            new_facts += 1
                            continue

        #Do inference without operators
        while new_facts:
            new_facts = 0
            while len(facts) > 0:
                i = facts.pop()
                if type(i) == list:
                    i = i[0]
                #Add facts to explored_facts
                explored_facts.append(i)
                if i in rules:
                    for k in rules[i]:
                        #Iterate each rules
                        if k not in facts and k not in explored_facts:
                            facts.append(k)
                            new_facts += 1
        
        #Collect all the facts into list
        response['facts'] = [response['name'] + "s " + i for i in explored_facts if "taxon_" not in i]
        for i in facts:
            response['facts'].append(response['name'] + " " + i)

    response['suggest'] = suggest
    return render(request, 'search.html',response)