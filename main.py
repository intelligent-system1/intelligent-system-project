'''
Intelligent System Project
by 
Fransiscus Emmanuel Bunaren
Muhammad Ivan Taftazani
Oliver Muhammad Fadhlurrahman
Yafi Ahsan Hakim
'''

#Import Wikidata Library for Python
from wikidata.client import Client

#Initializing Wikidata Client
client = Client()

#Get Wikidata Content from ID
wiki_id = input("Please type Wikidata ID :")
if not wiki_id:
    wiki_id = 'Q22686'
entity = client.get(wiki_id, load=True)

#Function fo Reading Predicate Info
def read_predicate(entity,n):
    try:
        data = entity[client.get(n)]
        data = client.get(data.id,load=True)
        return data
    except:
        return None

print("Instance of :",read_predicate(entity,'P31').label)
print("Siblings :",entity[client.get('P3373')].label)
print("Description :", entity.description)

print("\nImage Information :")
image_prop = client.get('P18')
image = entity[image_prop]
print(image)
print("Resolution :",image.image_resolution)
print("Image URL : " , image.image_url)
