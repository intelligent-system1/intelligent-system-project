# Intelligent System Project
# Rule Reasoning Over WikiData

## Project Description
Nowadays, data is an important resource for many organizations. It is predicted that in the future , the need for automatic data processing is increasing. Our team is planning to make a program that can automatically infer information from wikidata article(s) and yield informative results by performing logical reasoning using forward chaining method.

## Project Scope
WikiData contains a really large number of data, thus it’s not possible for our project to process all of that data within the given time. In order to minimize the scope of data for our input that we get from WikiData, we decided to minimize the topic of our data we use for our project to only the people section of WikiData.

## Logical Rules : 
For complete list of rules that is used in this application, please check !(RULES.md) file

## Implemented AI Techniques
On the process of making inferences from wikidata using logical reasoning, the program will implement Forward Chaining.
Forward chaining starts with the available data and uses inference rules to extract more data until a certain goal is reached. This method searches the inference rules until it finds one if-clause that returns true. When it is found, new information is inferred based on the then-clause. This process will be iterated until a goal is reached.